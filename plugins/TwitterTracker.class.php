<?php
/**
 * @file
 * Twitter tracker block
 */

class TwitterTracker extends BeanPlugin {

  /**
   * Declares default block settings.
   */
  public function values() {
    $values = parent::values();
    $values['id'] = 0;
    $values['event'] = 'PageView';
    return $values;
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {

    $form = array();

    $form['id'] = array(
      '#type' => 'textfield',
      '#title' => t('The Twitter Tracker ID.'),
      '#description' => t("This identifier can be found in the code Twitter provides on the line twq('init', 'xxxxxxxxx');"),
      '#required' => TRUE,
      '#default_value' => $bean->id,
    );

    $form['event'] = array(
      '#type' => 'select',
      '#title' => t('Event to track'),
      '#options' => array(
        'PageView' => t('View Content'),
      ),
      '#default_value' => $bean->event,
      '#required' => FALSE,
      '#multiple' => FALSE,
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $values = array(
      '#theme' => 'twitter_tracker',
      '#cache' => DRUPAL_CACHE_GLOBAL,
      'bean' => $content['bean'], // Needed by moriarty_preprocess_block
      'id' => $bean->id,
      'event' => $bean->event,
    );
    return $values;
  }
}
