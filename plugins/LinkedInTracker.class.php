<?php
/**
 * @file
 * LinkedIn tracker block
 */

class LinkedInTracker extends BeanPlugin {

  /**
   * Declares default block settings.
   */
  public function values() {
    $values = parent::values();
    $values['pid'] = '';
    return $values;
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = array();
    $form['pid'] = array(
      '#type' => 'textfield',
      '#title' => t('LinkedIn partner ID'),
      '#description' => t('This number can be found in the code LinkedIn provides on the line _linkedin_data_partner_id = "XXXXX";'),
      '#required' => TRUE,
      '#default_value' => $bean->pid,
    );

    return $form;
  }

  /**
   * Form validation
   */
  public function validate($values, &$form_state) {
    // @assumption - linkedin partner ID is always numeric
    if (!is_numeric($values['pid'])) {
      form_set_error('pid', 'Invalid partner ID.');
    }
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $values = array(
      '#theme' => 'linkedin_tracker',
      '#cache' => DRUPAL_CACHE_GLOBAL,
      'bean' => $content['bean'], // Needed by moriarty_preprocess_block
      'pid' => $bean->pid,
    );
    return $values;
  }
}
