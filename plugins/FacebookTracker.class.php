<?php
/**
 * @file
 * Facebook tracker block
 */

class FacebookTracker extends BeanPlugin {

  /**
   * Declares default block settings.
   */
  public function values() {
    $values = parent::values();
    $values['pixel'] = 0;
    $values['event'] = 'ViewContent';
    return $values;
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {

    $form = array();

    $form['pixel'] = array(
      '#type' => 'textfield',
      '#title' => t('The ID of your Facebook Pixel.'),
      '#description' => t("This number can be found in the code Facebook provides on the line fbq('init', 'xxxxxxxxxx');"),
      '#required' => TRUE,
      '#default_value' => $bean->pixel,
    );

    $form['event'] = array(
      '#type' => 'select',
      '#title' => t('Event to track'),
      '#options' => array(
        'ViewContent' => t('View Content'),
        'Search' => t('Search'),
        'Lead' => t('Lead'),
      ),
      '#default_value' => $bean->event,
      '#required' => FALSE,
      '#multiple' => FALSE,
    );

    return $form;
  }

  /**
   * Form validation
   */
  public function validate($values, &$form_state) {
    // @assumption - facebook pixel ID is always numeric
    if (!is_numeric($values['pixel'])) {
      form_set_error("pixel", "Invalid pixel ID.");
    }
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $values = array(
      '#theme' => 'facebook_tracker',
      '#cache' => DRUPAL_CACHE_GLOBAL,
      'bean' => $content['bean'], // Needed by moriarty_preprocess_block
      'pixel' => $bean->pixel,
      'event' => $bean->event,
    );
    return $values;
  }
}
