<?php
/**
 * @file
 * Google Adwords tracking block
 */

class AdwordsTracker extends BeanPlugin {

  /**
   * Declares default block settings.
   */
  public function values() {
    $values = parent::values();
    $values['id'] = 0;
    $values['pixel_type'] = 'conversion';
    $values['lang'] = 'en';
    $values['format'] = '2';
    $values['colour'] = 'FFFFFF';
    $values['conversion_label'] = '';
    return $values;
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {

    $form = array();

    $form['id'] = array(
      '#type' => 'textfield',
      '#title' => t('Conversion ID'),
      '#description' => t("This number can be found in the code Adwords provides."),
      '#required' => TRUE,
      '#default_value' => $bean->id,
    );

    $form['pixel_type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => array(
        'conversion' => t('Conversion'),
        'remarketing' => t('Remarketing'),
      ),
      '#default_value' => $bean->pixel_type,
      '#description' => t('More information about conversion tracking <a href="@url-conversion">here</a>. More information about remarketing tags <a href="@url-remarketing">here</a>.', array('@url-conversion' => 'https://support.google.com/adwords/answer/7013781', '@url-remarketing' => 'https://support.google.com/adwords/answer/2476688')),
    );

    $form['lang'] = array(
      '#type' => 'select',
      '#title' => t('Conversion language'),
      '#options' => array(
        'en' => t('English'),
        'fr' => t('French'),
      ),
      '#default_value' => $bean->lang,
      '#states' => array(
        'visible' => array(
          ':input[name="pixel_type"]' => array('value' => 'conversion'),
        ),
        'required' => array(
          ':input[name="pixel_type"]' => array('value' => 'conversion'),
        ),
      ),
    );

    $form['format'] = array(
      '#type' => 'select',
      '#title' => t('Conversion Format'),
      '#options' => array(
        '1' => t('1 line notification to visitors'),
        '2' => t('2 line notification to visitors'),
        '3' => t('No notification'),
      ),
      '#default_value' => $bean->format,
      '#description' => t('What you want to display on your page.'),
      '#states' => array(
        'visible' => array(
          ':input[name="pixel_type"]' => array('value' => 'conversion'),
        ),
        'required' => array(
          ':input[name="pixel_type"]' => array('value' => 'conversion'),
        ),
      ),
    );

    $form['colour'] = array(
      '#type' => 'textfield',
      '#title' => t('Conversion Colour'),
      '#default_value' => $bean->colour,
      '#size' => 15,
      '#maxlength' => 255,
      '#description' => t('Enter a valid HTML colour (named or hex value)'),
      '#states' => array(
        'visible' => array(
          ':input[name="pixel_type"]' => array('value' => 'conversion'),
        ),
        'required' => array(
          ':input[name="pixel_type"]' => array('value' => 'conversion'), // @see validate().
        ),
      ),
    );

    // BeanPlugin already populates $form['label'] so we need to use another key: $form['conversion_label'].
    $form['conversion_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Conversion Label'),
      '#default_value' => $bean->conversion_label,
      '#size' => 15,
      '#maxlength' => 255,
      '#description' => t('This can be found in the code Adwords provides.'),
      '#states' => array(
        'visible' => array(
          ':input[name="pixel_type"]' => array('value' => 'conversion'),
        ),
        'required' => array(
          ':input[name="pixel_type"]' => array('value' => 'conversion'), // @see validate().
        ),
      ),
    );

    return $form;
  }

  /**
   * Form validation
   */
  public function validate($values, &$form_state) {
    // @assumption - google conversion ID is always numeric
    if (!is_numeric($values['id'])) {
      form_set_error("id", "Invalid conversion ID.");
    }
    // Workaround for https://www.drupal.org/node/2405271
    // Make sure conditionally required fields are indeed required.
    if ($values['pixel_type'] == 'conversion') {
      if (empty($values['colour'])) {
        form_set_error('colour', t('!name field is required.', array('!name' => 'Conversion Colour')));
      }
      if (empty($values['conversion_label'])) {
        form_set_error('conversion_label', t('!name field is required.', array('!name' => 'Conversion Label')));
      }
      $named = array('aliceblue', 'antiquewhite', 'aqua', 'aquamarine', 'azure', 'beige', 'bisque', 'black', 'blanchedalmond', 'blue', 'blueviolet', 'brown', 'burlywood', 'cadetblue', 'chartreuse', 'chocolate', 'coral', 'cornflowerblue', 'cornsilk', 'crimson', 'cyan', 'darkblue', 'darkcyan', 'darkgoldenrod', 'darkgray', 'darkgreen', 'darkkhaki', 'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid', 'darkred', 'darksalmon', 'darkseagreen', 'darkslateblue', 'darkslategray', 'darkturquoise', 'darkviolet', 'deeppink', 'deepskyblue', 'dimgray', 'dodgerblue', 'firebrick', 'floralwhite', 'forestgreen', 'fuchsia', 'gainsboro', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'greenyellow', 'honeydew', 'hotpink', 'indianred', 'indigo', 'ivory', 'khaki', 'lavender', 'lavenderblush', 'lawngreen', 'lemonchiffon', 'lightblue', 'lightcoral', 'lightcyan', 'lightgoldenrodyellow', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen', 'lightskyblue', 'lightslategray', 'lightsteelblue', 'lightyellow', 'lime', 'limegreen', 'linen', 'magenta', 'maroon', 'mediumaquamarine', 'mediumblue', 'mediumorchid', 'mediumpurple', 'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise', 'mediumvioletred', 'midnightblue', 'mintcream', 'mistyrose', 'moccasin', 'navajowhite', 'navy', 'oldlace', 'olive', 'olivedrab', 'orange', 'orangered', 'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise', 'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink', 'plum', 'powderblue', 'purple', 'red', 'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen', 'seashell', 'sienna', 'silver', 'skyblue', 'slateblue', 'slategray', 'snow', 'springgreen', 'steelblue', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'wheat', 'white', 'whitesmoke', 'yellow', 'yellowgreen');
      if (!in_array(strtolower($values['colour']), $named)) {
        if (!preg_match('/^[a-f0-9]{6}$/i', strtolower($values['colour']))) {
          form_set_error("colour", "Invalid HTML colour");
        }
      }
    }
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $values = array(
      '#theme' => ($bean->pixel_type == 'conversion') ? 'adwords_tracker_conversion' : 'adwords_tracker_remarketing',
      '#cache' => DRUPAL_CACHE_GLOBAL,
      'bean' => $content['bean'], // Needed by moriarty_preprocess_block
      'id' => $bean->id,
      'pixel_type' => $bean->pixel_type,
      'lang' => $bean->lang,
      'format' => $bean->format,
      'colour' => $bean->colour,
      'conversion_label' => $bean->conversion_label,
    );
    return $values;
  }
}
