<?php
/**
 * @file
 * Doubleclock tracking block
 */

class DoubleclickTracker extends BeanPlugin {

  /**
   * Declares default block settings.
   */
  public function values() {
    $values = parent::values();
    $values['id'] = 0;
    $values['activity'] = 'Doubleclick campaign';
    $values['url'] = '';
    $values['creation'] = '';
    return $values;
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {

    $form = array();

    $form['id'] = array(
      '#type' => 'textfield',
      '#title' => t('Campaign ID'),
      '#description' => t("This number can be found in the code Doubleclick provides."),
      '#required' => TRUE,
      '#default_value' => $bean->id,
    );

    $form['activity'] = array(
      '#type' => 'textfield',
      '#title' => t('Activity'),
      '#description' => t("The activity being tracked."),
      '#default_value' => $bean->activity,
      '#required' => FALSE,
    );

    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL being tracked'),
      '#default_value' => $bean->url,
      '#required' => FALSE,
      '#description' => t('The website this tracker will be on.'),
    );

    $form['creation'] = array(
      '#type' => 'textfield',
      '#title' => t('Creation date'),
      '#default_value' => $bean->creation,
      '#size' => 15,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#description' => t('The date you created this campaign.'),
    );

    return $form;
  }

  /**
   * Form validation
   */
  public function validate($values, &$form_state) {
    // @assumption - doubleclick conversion ID is always numeric
    if (!is_numeric($values['id'])) {
      form_set_error("id", "Invalid campaign ID.");
    }
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $values = array(
      '#theme' => 'doubleclick_tracker',
      '#cache' => DRUPAL_CACHE_GLOBAL,
      'bean' => $content['bean'], // Needed by moriarty_preprocess_block
      'id' => $bean->id,
      'activity' => $bean->activity,
      'url' => $bean->url,
      'creation' => $bean->creation,
    );
    return $values;
  }
}
