<?php
/**
 * @file
 * Google Analytics tracker block
 */

class GoogleAnalyticsTracker extends BeanPlugin {

  /**
   * Declares default block settings.
   */
  public function values() {
    $values = parent::values();
    $values['id'] = 0;
    $values['event'] = 'pageview';
    $values['extraevents'] = NULL;
    return $values;
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {

    $form = array();

    $form['id'] = array(
      '#type' => 'textfield',
      '#title' => t('The Google Analytics tracking ID.'),
      '#description' => t("This identifier can be found in the code Google Analytics provides on the line \"ga('create', 'XX-XXXXX-X', 'auto');\""),
      '#required' => TRUE,
      '#default_value' => $bean->id,
    );

    // Choose from the available analytics.js hit types.
    // @see https://developers.google.com/analytics/devguides/collection/analyticsjs/field-reference#hitType
    $form['event'] = array(
      '#type' => 'select',
      '#options' => array(
        'pageview' => t('View Content'),
        'screenview' => t('Screen View'),
        'event' => t('Event'),
        'transaction' => t('Transaction'),
        'item' => t('Item'),
        'social' => t('Social'),
        'exception' => t('Exception'),
        'timing' => t('Timing'),
      ),
      '#default_value' => $bean->event,
      '#required' => FALSE,
      '#multiple' => FALSE,
    );

   // Field for User Interaction Events (register a specific set of events for this elements on this page).
   // See https://developers.google.com/analytics/devguides/collection/analyticsjs/events
    $form['extraevents'] = array (
     '#type' => 'textarea',
     '#title' => t('Specify interactive events'),
     '#description' => t('Use Events to <a href="@google-docs">collect Google Analytics data</a> on interactions with your content.
 Uses the format [eventCategory], [eventAction], [eventLabel] | [list of DOM element event triggers], e.g.\'call-to-action\', \'click\', \'Fall Campaign\'| a.readmore, .region-header .cta ',
       array('@google-docs' => 'https://support.google.com/analytics/answer/1033068#Anatomy')),
     '#required' => FALSE,
     '#default_value' => $bean->extraevents,
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $values = array(
      '#theme' => 'googleanalytics_tracker',
      '#cache' => DRUPAL_CACHE_GLOBAL,
      'bean' => $content['bean'], // Needed by moriarty_preprocess_block
      'id' => $bean->id,
      'event' => $bean->event,
      'extraevents' => $bean->extraevents,
    );

    // Only attach the extraevents JS if there are any.
    if (!empty($bean->extraevents)) {
      // Need to send extraevents to JS via attached method.
      $values['#attached'] = array (
        'js' => array(
          // Loads the behaviour for tracking extra events.
            drupal_get_path('module', 'campaign_tracker') . '/js/googleAnalyticsExtraEvents.js',
          // Adds the extraEvents data to the Drupal.settings global object.
          array(
            'type' => 'setting',
            'data' => array('googleAnalyticsTrackerExtras' => array('extraEvents' => explode("\n", $bean->extraevents))),
          ),
        ),
    );
    };

    return $values;
  }
}
