<?php
/**
 * @file
 * Google Tag Manager tracker block
 */

class GoogleTagManagerTracker extends BeanPlugin {

  /**
   * Declares default block settings.
   */
  public function values() {
    $values = parent::values();
    $values['id'] = 0;
    return $values;
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {

    $form = array();

    $form['id'] = array(
      '#type' => 'textfield',
      '#title' => t('The Google Tag Manager tag ID.'),
      '#description' => t("This identifier can be found in the code Google Tag Manager provides on the line \"(window,document,'script','dataLayer','XXX-XXXXXXX);\""),
      '#required' => TRUE,
      '#default_value' => $bean->id,
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $gtm_script = <<<EOD
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','{$bean->id}');
EOD;
    $values = array(
      '#theme' => 'google_tag_manager_tracker',
      // Using #attached will get the script in the head of the HTML.
      '#attached' => array(
        'js' => array(
          array(
            'data' => $gtm_script,
            'type' => 'inline',
          ),
        ),
      ),
      '#cache' => DRUPAL_CACHE_GLOBAL,
      'bean' => $content['bean'], // Needed by moriarty_preprocess_block
      'id' => $bean->id,
    );
    return $values;
  }
}
