/**
 * Selects page elements and associates a click event handler to them, pushing the event to GA. 
 * See: https://developers.google.com/analytics/devguides/collection/analyticsjs/events.
 */

(function($) {
  Drupal.behaviors.googleAnalyticsExtraEvents = {
    attach: function(context, settings) {
      // An array of strings representing the actions to track and the page elements that will trigger them, provided from the Drupal.settings obj.
      var gaEvents = Drupal.settings.googleAnalyticsTrackerExtras.extraEvents;

      // For each event in the array, extract the event itself and the DOM element(s) to trigger it.
      $.each(gaEvents, function(i, eventString) {
        var event = {};
        event.details = eventString.split("|")[0];
        event.elements = eventString.split("|")[1];
        
        // Split the details further into an array...
        var eD = event.details.split(',');

        // Add those details into a single array of info in the right format to push to Google.
        var eventData = ['send', 'event'].concat(eD);

        // Bind a click event to each of event.elements.
        $(event.elements).bind('click', function(e) {
          // Trigger Google Analytics send event using apply for eventData params.
          ga.apply(this, eventData);
        });
      });
    }
  };
})(jQuery);
